import json
import re

from azure.cli.core import get_default_cli

cli = get_default_cli()

with open('./config.json') as c:
    config = json.load(c)


def setup_login():
    cli.invoke(['login'])
    result = cli.result.result[0]

    return result['id'], result['tenantId']


def create_app():
    cli.invoke(['ad', 'sp', 'create-for-rbac', '-n', config['AD_APP_NAME']])
    result = cli.result.result

    return result['appId']


def reset_app_secret(client_id):
    cli.invoke(['ad', 'app', 'credential', 'reset', '--id', client_id])
    result = cli.result.result

    return result['password']


def create_resource_group():
    cli.invoke(['group', 'create', '--name', config['RESOURCE_GROUP_NAME'], '--location', config['RESOURCE_GROUP_LOCATION']])


def generate_template(account_information):
    with open('./baseTemplate.json') as f:
        template = f.read()

    result = set(re.findall("\$\{(.*?)}", template))
    template_values = {**account_information, **config}

    for group in result:
        if group in template_values:
            template = template.replace("${" + group + "}", template_values[group])

    with open('./deploymentTemplate.json', 'w') as d:
        d.write(template)

    return './deploymentTemplate.json'


def deploy(template_path):
    cli.invoke(['group', 'deployment', 'create', '--resource-group', config['RESOURCE_GROUP_NAME'],
                '--template-file', template_path])


if __name__ == "__main__":
    print("-----1/5 (LOGIN)-----")
    subscription_id, tenant_id = setup_login()
    print("-----1/5 (DONE)-----")

    if not all([False for value in ["CLIENT_ID", "CLIENT_SECRET"] if value not in config]):
        print("-----2/5 (CREATING APP)-----")
        client_id = create_app()
        print("-----2/5 (DONE)-----")

        print("-----3/5 (RESETTING APP SECRET)-----")
        client_secret = reset_app_secret(client_id)
        print("-----3/5 (DONE)-----")
    else:
        client_id = config["CLIENT_ID"]
        client_secret = config["CLIENT_SECRET"]
        print("-----SKIPPING TO STEP 4-----")

    print("-----4/5 (CREATING RESOURCE GROUP)-----")
    create_resource_group()
    print("-----4/5 (DONE)-----")

    print("-----5/5 (DEPLOYING)-----")
    account_information = {
        "CLIENT_ID": client_id,
        "CLIENT_SECRET": client_secret,
        "SUBSCRIPTION_ID": subscription_id,
        "TENANT_ID": tenant_id
    }

    file_path = generate_template(account_information)
    deploy(file_path)
    print("-----5/5 (DONE)-----")
